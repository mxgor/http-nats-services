package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"http-nats-services/presentation"
)

const NatsUrl = "localhost:4222"
const ServicePort = 8080

func main() {
	server := gin.Default()
	presentation.RequestPresentationRoute(server, NatsUrl)
	server.Run(fmt.Sprintf(":%d", ServicePort))
}
